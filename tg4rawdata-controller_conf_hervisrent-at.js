tgNamespace.tgContainer.registerInnerConfig(
    "tg4raw_controller",
    //LIVE CONF
    {
        countConfigs: 1,
        countFuncLibs: 0,

        oTrackDomains : {
            A : "//jtsp.hervisrent.at"
        },
        aTrackDomainsListing : ["A"],

        account : "www-hervisrent-at",

        lsDomain : "jentis_raw_controller_domains",
        lsDomainDuration : 60*60*24*365*2,

        lsParents : "jentis_raw_controller_parents",
        lsParentsDuration : 60*60*24*365*2,

        lsDocumentIds : "jentis_raw_controller_documentids",
        lsDocumentIdsDuration : 60*60*24*365*2
    },
    //TEST CONFIG
    {
        account : "testserver-hervisrent-at"
    }
)
;