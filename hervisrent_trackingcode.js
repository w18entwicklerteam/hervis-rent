jQuery(document).ready(function(){

    var getIframe = jQuery("iframe");
    var easyRent = false;

    if(getIframe.length !== 0){
        for (var i = 0; i < getIframe.length; i++)
        {
            var iframeSrc = getIframe[i].attributes.src.nodeValue;
            if(iframeSrc.includes("easyresv3") === true){
                easyRent = true;
            }

        }


    }

    if((easyRent === false || getIframe.length === 0) && document.referrer.includes("easyrent")!== true){

        _trackgate.push({
            track           : "pageview",
            pagegroup       : ["hervisrent"]
        });


        tgNamespace.tgContainer.registerAnonymReady("pageviewBeforeFinalAsync");

    }






    window.addEventListener("message", function(evt){

        if(typeof JSON.parse(evt.data).track !== "undefined"){

            _trackgate.push(JSON.parse(evt.data));
            if(JSON.parse(evt.data).track === "pageview"){

                tgNamespace.tgContainer.registerAnonymReady("pageviewBeforeFinalAsync");
            }

        }


    }, false);

});