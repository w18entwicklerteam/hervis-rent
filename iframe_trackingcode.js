var _trackgate = {
    "push": function (aData) {

        window.parent.postMessage(JSON.stringify(aData), "*");
    }
};


var aShopId = /.*shop_id=([^&]*).*/.exec(document.location.search);
if (aShopId !== null) {
    var sShopId = aShopId[1];
}
else {
    var sShopId = "no";
}


_trackgate.push({
    track: "pagevar",
    key: "shopid",
    value: sShopId
});


var sReservierungID = /.*r_intnr_reservierung=([^&]*).*/.exec(document.location.search);

var currentURL = window.location.href;


if ((window.location.href).includes("_intnr_reservierung")) {

    this.iFrame = document.createElement("iframe");
    this.iFrame.width = 1;
    this.iFrame.height = 1;
    this.iFrame.scrolling = "no";
    this.iFrame.frameborder = 0;
    this.iFrame.style = "display:none";
    this.iFrame.id = "HervisRentiFrame";


    var iframe_url = "//www.hervisrent.at"

    this.iFrame.src = encodeURI(iframe_url);

    // Append and Fire iFrame
    var oFirstScript = document.getElementsByTagName("script")[0];
    oFirstScript.parentNode.insertBefore(this.iFrame, oFirstScript);


    $("#HervisRentiFrame").on('load', function () {

        var _trackgate = {
            "push": function (aData) {
                $("iframe[src='//www.hervisrent.at']")[0].contentWindow.postMessage(JSON.stringify(aData), "*");
            }
        };

        _trackgate.push({
            track: "eventvar",
            key: "reservierungsid",
            value: sReservierungID
        });


        _trackgate.push({
            track: "event",
            group: "reservierung",
            name: sShopId,
            value: localStorage.getItem("w18_sFilialId")
        });


        _trackgate.push({
            track: "product",
            id: "007",
            name: "Hervis_rent",
            brutto: sum_rental,
            type: "sale"
        });

        _trackgate.push({
            track: "sale",
            orderid: "008",
            brutto: sum_rental
        });


        _trackgate.push({
            track: "pageview",
            pagetitle: "/buchung/6_bestaetigung_ausdrucken",
            pagegroup: ["wintersteigerbuchung"]
        });


    });


}


var virtuellerPageview;
var sNavId = jQuery(".checkout-bar").find(".active").attr("id");


switch (sNavId) {
    case "assi-li-0":
        virtuellerPageview = "1_zeitraum_standort";
        break;
    case "assi-li-1":
        virtuellerPageview = "2_produktauswahl";
        break;
    case "assi-li-2":
        virtuellerPageview = "3_angabe_besteller";
        break;
    case "assi-li-3":
        virtuellerPageview = "4_pruefung_buchung";
        break;
    case "assi-li-4":
        virtuellerPageview = "5_eingabe_bezahlung";

}


_trackgate.push({
    track: "pageview",
    pagetitle: "/buchung/" + virtuellerPageview,
    pagegroup: ["wintersteigerbuchung"]
});


_trackgate.push({
    track: "submit"
});


$(window).on('load', function () {

    if (jQuery("#point_8").find("input[type='radio']").length !== 0) {
        localStorage.setItem("w18_sFilialId", $("#point_8").find("input[type='radio']:checked").next().text().trim());

        $("#point_8").find("input[type='radio']").on("change", function () {

            if ($(this).prop("checked") === true) {
                localStorage.setItem("w18_sFilialId", $(this).next().text().trim());
            }
        });

    }
});


